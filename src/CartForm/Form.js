import React from "react";
import { Formik } from "formik";
import * as yup from "yup";
import "./CartStylesWithForm.css";



function Form (props) {
    const validationSchema = yup.object().shape({
        name: yup.string().min(2, 'Must be 2 characters or more').typeError('Должно быть строкой').required('Введите имя'),
        secondName: yup.string().min(2, 'Must be 2 characters or more').typeError('Должно быть строкой').required('Введите фамилию'),
        age: yup.number().min(16, 'Must be 16 years or more').typeError('Должно быть число').required('Введите возраст'),
        adress: yup.string().typeError('Должно быть строка и номер').required('Введите адрес'),
        telephone: yup.number().typeError('Должно быть число').required('Введите номер телефона'),
    })

    const {removeAfterBuy} = props;

    const productsForBuy =  localStorage.getItem('cart');
    const products = JSON.parse(productsForBuy);
    
    return(
        <div>
            <Formik
                initialValues = {{
                    name: "",
                    secondName: "",
                    age: "",
                    adress: "",
                    telephone: "",
                    products: products
                    }}
                validateOnBlur
                onSubmit={(values,{resetForm}) => { 
                    console.log(values);
                    removeAfterBuy();
                    resetForm();
                    
                     }}
                validationSchema={validationSchema}
                >
                {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
                    <div name="contact_form" className="form">
                        
                        <p>
                            <label className={"label"} htmlFor={"name"}>Имя</label><br />
                            <input 
                            className={"input"}
                            type={"text"} 
                            name={"name"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.name}
                            />
                        </p>
                        {touched.name && errors.name && <p className={"error"}> {errors.name}</p>}

                        <p>
                            <label className={"label"} htmlFor={"secondName"}>Фамилия</label><br />
                            <input 
                            className={"input"}
                            type={"text"} 
                            name={"secondName"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.secondName}
                            />
                        </p>
                        {touched.secondName && errors.secondName && <p className={"error"}> {errors.secondName}</p>}

                        <p>
                            <label className={"label"} htmlFor={"age"}>Возраст</label><br />
                            <input 
                            className={"input"}
                            type={"text"} 
                            name={"age"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.age}
                            />
                        </p>
                        {touched.age && errors.age && <p className={"error"}> {errors.age}</p>}

                        <p>
                            <label className={"label"} htmlFor={"adress"}>Адрес</label><br />
                            <input 
                            className={"input"}
                            type={"text"} 
                            name={"adress"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.adress}
                            />
                        </p>
                        {touched.adress && errors.adress && <p className={"error"}> {errors.adress}</p>}

                        <p>
                            <label className={"label"} htmlFor={"telephone"}>Номер телефона</label><br />
                            <input 
                            className={"input"}
                            type={"tel"} 
                            name={"telephone"}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values.telephone}
                            />
                        </p>
                        {touched.telephone && errors.telephone && <p className={"error"}> {errors.telephone}</p>}

                        <button
                            className={"sumbmin"}
                            disabled={!isValid && !dirty}
                            onClick={handleSubmit}
                            type={"submit"}
                            >Купить</button>
                    </div>
                )    
                }

            </Formik>





        </div>
    )
}

export default Form;