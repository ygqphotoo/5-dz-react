import React from "react";
import "../ModalStyles/ModalFirstStyle.css";

function Modal(props){
    const {onSubmit, onCancel, isOpen} = props;
        return(
            <>
                {isOpen &&
                    <>
                        <div className="modal">
                            <div className="overflow" onClick={onCancel}></div>
                            <div className="modal_content" >
                                <button className="closeActive_first" onClick={onCancel} >X</button>
                                <h1 className="modal__title_first">{props.header}</h1>
                                <p className="modal__text_first">{props.text}</p>
                                <div className="btn__position">
                                    <button className="super_first" 
                                    onClick={() =>{onSubmit(props.id)}}>
                                            {props.actionsGood}</button>
                                    <button className="bad_first" onClick={onCancel}>{props.actionsBad}</button>
                                </div>
                            </div>
                        </div>
                    </>
                }
            </>
        )
    }

export default Modal;