import React from "react";
import Modal from "../Modal_component/Modal";
import "./Products.css";
import { Switch, Route } from "react-router-dom";
import { connect } from "react-redux"
import CartList from "../pages/cart/CartList";
import FavoriteList from "../pages/favorites/FavoritesList";
import PropsProducts from "./PropsProducts";
import {modalAddToggle, modalRemoveToggle, modalCurrentId} from "../store/modal/actions"

import {removeProductFromFavorite, 
    removeFromCart, 
    addProductToCart, 
    addProductToFavorite, 
    removeAfterBuy,
    getProducts as getProductsAction}  from "../store/products/actions"
import Form from "../CartForm/Form";

const mapStateToProps = (store) => {
    return{
        products: store.product.products,
        productsIsInCart: store.product.products.filter((product) => product.isInCart),
        productIsInFavorite: store.product.products.filter((product) => product.isInFavorites),
        isLoading: store.product.productLoad,
        error: store.product.error,
        isAddModalOpen: store.modal.isAddModalOpen,
        isRemove: store.modal.isRemoveModalOpen,
        currentProductId: store.modal.currentProductId,
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        removeAfterBuy: () => dispatch(removeAfterBuy()),
        getProducts: (id) => dispatch(getProductsAction(id)),
        addProductToCart: (id) => dispatch(addProductToCart(id)),
        removeFromCart: (id) => dispatch(removeFromCart(id)),
        addProductToFavorite: (id) => dispatch(addProductToFavorite(id)),
        removeProductFromFavorite: (id) => dispatch(removeProductFromFavorite(id)),
        modalAddToggle: (id) => dispatch(modalAddToggle(id)),
        modalRemoveToggle: (id) => dispatch(modalRemoveToggle(id)),
        // modalCurrentId: () => dispatch(modalCurrentId())
    }
}

export const LoadProducts = connect(mapStateToProps, mapDispatchToProps)((props) => {
const {products,
    currentProductId,
    removeFromCart,
    modalRemoveToggle,
    isRemove,
    isAddModalOpen,
    productIsInFavorite,
    productsIsInCart,
    addProductToFavorite,
    removeProductFromFavorite,
    modalAddToggle,
    addProductToCart,
    removeAfterBuy,
    modalCurrentId
} = props;
console.log(currentProductId);
        return(
            <div className="products"> 
                <Switch>
                    <Route exact path="/">
                        <PropsProducts product = {products}
                        handleModalOpen = {() => modalAddToggle()}
                        addProductToFavorite = {(currentProductId) => addProductToFavorite(currentProductId)}
                        removeProductFromFavorite = {(currentProductId) => removeProductFromFavorite(currentProductId)}
                    />
                    <Modal             
                        id = {currentProductId}
                        isOpen = {isAddModalOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {() =>{modalAddToggle()}}
                        onSubmit = {() =>{modalAddToggle(); addProductToCart(currentProductId)}}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                    </Route>
                <Route path="/cart">
                    <CartList
                        products = {productsIsInCart}
                        handleModalOpen = {(currentProductId) => {modalRemoveToggle(currentProductId)}}
                        addProductToFavorite = {(currentProductId) => {addProductToFavorite(currentProductId)}}
                        removeProductFromFavorite = {(currentProductId) => {removeProductFromFavorite(currentProductId)}}
                        buttonTitle = 'remove'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isRemove}
                        header = {"Хотите удалить с корзины?"}
                        onCancel = {() =>{modalRemoveToggle()}}
                        onSubmit = {() =>{
                            removeFromCart(currentProductId)
                            modalRemoveToggle()
                        }}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"} 
                    />
                    <Form 
                        removeAfterBuy={removeAfterBuy}
                    />

                </Route>
                <Route path="/favorite">
                    <FavoriteList
                        products = {productIsInFavorite}
                        handleModalOpen = {() => {modalAddToggle()}}
                        addProductToFavorite = {() => {addProductToFavorite(currentProductId)}}
                        removeProductFromFavorite = {(currentProductId) => {removeProductFromFavorite(currentProductId)}}
                        buttonTitle = 'add'
                    />
                    <Modal 
                        id = {currentProductId}
                        isOpen = {isAddModalOpen}
                        header = {"Хотите добавить в корзину?"}
                        onCancel = {() =>{modalAddToggle()}}
                        onSubmit = {() => {
                            modalAddToggle();
                            addProductToCart(currentProductId);
                        }}
                        actionsGood = {"Да"}
                        actionsBad = {"Нет"}                             
                    />
                </Route>
                </Switch>
            </div>
            
        )
        })
           
export default LoadProducts;