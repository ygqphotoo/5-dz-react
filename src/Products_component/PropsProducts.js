import React from "react";
import {Product} from "./RendProducts"

function PropsProducts (props){
        return (
            <>
                {
                    props.product &&
                    props.product.map((product) => {
                        // console.log(product.id);
                        return (
                            <Product 
                                key = {product.id}
                                title = {product.name}
                                price = {product.price}
                                url = {product.url}
                                article = {product.article}
                                color = {product.color}
                                id = {product.id}
                                handleModalOpen = {props.handleModalOpen}
                                addProductToFavorite = {props.addProductToFavorite}
                                removeProductFromFavorite = {props.removeProductFromFavorite}
                                isInFavorites = {product.isInFavorites}
                            />
                        );
                    })
                }
            </>
        )
    }



export default PropsProducts;