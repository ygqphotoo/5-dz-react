import React from "react";
import {AiOutlineStar} from "react-icons/ai";



export function Cart (props){
    const {product, handleModalOpen, removeProductFromFavorite, addProductToFavorite} = props;
    return(
                    <div key={product.id} className="product_position">
                        <button className="btn_delete"
                                onClick= {() => {handleModalOpen(product.id)}}
                        >X</button>
                        <div className="products_set">
                            <p className="product_name">{product.name}</p>
                            <p className="product_price">{product.price}</p>
                            <img className="product_img" src={product.url} alt="product_image"/>
                            <p className="product_article">Article: {product.article}</p>
                            <p className="product_color">Color: {product.color}</p>
                        </div>
                        <AiOutlineStar
                            onClick = {() => product.isInFavorites ? removeProductFromFavorite(product.id) : addProductToFavorite(product.id)}
                            className = {product.isInFavorites ? "favorite": "noFavorite"}/>
                    </div>
    )

}