import React from "react";
import {Cart} from "./Cart";
import "../../Products_component/Products.css"


function CartList (props){
    return(
            <>
            {props.products && props.products.map((product)  => (
                    <Cart
                        product={product}
                        key={product.id}
                        {...props}
                    />
            ))
            }
            </>
    )
}

export default CartList;