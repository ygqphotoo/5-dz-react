import React from 'react';
import {AiOutlineStar} from "react-icons/ai";
import Button from "../../Button_component/Button";
import {connect} from "react-redux";
import {modalAddToggle} from "../../store/modal/actions"

const mapDispatchToProps = (dispatch) => {
    return {
        modalAddToggle: (id) => dispatch(modalAddToggle(id)),
    }}

    export const Favorite = connect(null, mapDispatchToProps)((props) => {

    const {product, modalAddToggle, removeProductFromFavorite} = props;
 
   return (
        <div className="product_position">
            <div className="products_set">
                <p className="product_name">{product.name}</p>
                <p className="product_price">{product.price}</p>
                <img className="product_img" src={product.url} alt="product_image"/>
                <p className="product_article">Article: {product.article}</p>
                <p className="product_color">Color: {product.color}</p>
                <Button
                    className="propsBtn"
                    onClick = {() => {modalAddToggle(product.id)}}
                    text = {"Add to cart"}>
                </Button>
                <AiOutlineStar
                    onClick = {() => removeProductFromFavorite(product.id)}
                    className = {product.isInFavorites ? "favorite": ""}
                />
            </div>
        </div>
    
    )
})
