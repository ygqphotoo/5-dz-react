import React from "react";
import {Favorite} from "./Favorite";
import "../../Products_component/Products.css";

function FavoriteList (props) {
    return(
        <>
            {props.products && props.products.map((product) => (
                <Favorite
                    key={product.id}
                    product={product}
                    {...props}
                />
            ))
            }
        </>
    )
}

export default FavoriteList;