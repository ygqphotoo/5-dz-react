import { MODAL_ADD_TOGGLE, MODAL_REMOVE_TOGGLE, MODAL_CURRENT_ID } from './types'


export const modalAddToggle = (id) => {
    // console.log(id);
    return{
        type: MODAL_ADD_TOGGLE,
        payload: {id},
   }
}

export const modalRemoveToggle = (id) => {   
    return{
        type: MODAL_REMOVE_TOGGLE,
        payload: {id},
    }
}

export const modalCurrentId = (id) => {
    return{
        type: MODAL_CURRENT_ID,
        payload: {id}
    }
}