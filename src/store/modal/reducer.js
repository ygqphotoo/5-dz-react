import { MODAL_ADD_TOGGLE, MODAL_CURRENT_ID, MODAL_REMOVE_TOGGLE } from './types'


const initialState = {
    isAddModalOpen: false,
    isRemoveModalOpen: false,
    currentProductId: null,
}

export const modalReducer = (state = initialState , action) => {
    switch (action.type){
        case MODAL_ADD_TOGGLE:  
        console.log("add toggle work");
          
                return{
                    ...state,
                    isAddModalOpen: !state.isAddModalOpen,
                    currentProductId: action.payload.id
                }
            
            case MODAL_REMOVE_TOGGLE:               
                    return{
                        ...state,                       
                        isRemoveModalOpen: !state.isRemoveModalOpen,
                        currentProductId: action.payload.id
                    }


            case MODAL_CURRENT_ID:
                console.log("add current work");
                // console.log(state);
                return{
                    ...state,
                    currentProductId: action.payload.id
                }        
                    default:
                        return state;
            }    
        }


 
 