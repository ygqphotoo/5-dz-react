import {GET_PRODUCTS_INIT, 
        GET_PRODUCTS_ERROR, 
        GET_PRODUCTS_SUCCESS, 
        TOGGLE_PRODUCT_TO_CART, 
        TOGGLE_PRODUCT_TO_FAVORITES} from "./types"
import { store } from "../index"

const getProductsError = (error) => {
    return {
        type: GET_PRODUCTS_ERROR, 
        payload: error
    }
}

const getProductsInit = () =>{
    return {
        type: GET_PRODUCTS_INIT 
    }
}

const getProductsSuccess = (products) =>{
    console.log('getProductsSuccess => ', products);
    return{
        type: GET_PRODUCTS_SUCCESS,
        payload: products
    }
}

const  checkProducts = (products, cart, favorite) => {
    const updatedProducts = products.map(product =>{
        const productInCart = cart.some(({id}) => id === product.id)
        const productInFavorite = favorite.some(({id}) => id === product.id)
        return {
            ...product,
            isInCart: productInCart, 
            isInFavorites: productInFavorite
        }
    })
    return updatedProducts
}

function updatedStorage (updatedProducts) {
    updatedProducts.forEach(element => {
        if(element.isInCart === "true"){
            localStorage.setItem("cart", JSON.stringify(updatedProducts));
        }
    });
}

export const addProductToCart = (id) => (dispatch)  => {
    const state = store.getState();
    const products = state.product.products;
    const cart = JSON.parse(localStorage.getItem("cart"))
    const product = products.find((item) => item.id === id)
    cart.push(product)

    dispatch(getProducts());

    localStorage.setItem("cart", JSON.stringify(cart))

    return {
        type: TOGGLE_PRODUCT_TO_CART,
        payload: products.id
        /// was payload: product;
    }
}


export const addProductToFavorite = (id) => (dispatch) =>{
    const state = store.getState();
    const products = state.product.products;
    const favorite = JSON.parse(localStorage.getItem("favorite"))
    const product = products.find((product) => product.id === id)          
    favorite.push(product)

    dispatch(getProducts());

    localStorage.setItem("favorite", JSON.stringify(favorite))

        return {
            type: TOGGLE_PRODUCT_TO_FAVORITES,
            payload: product     
        }
    }
    
export const  removeProductFromFavorite = (id) => (dispatch) => {
    const state = store.getState();
    const products = state.product.products;
    
    const favorite = JSON.parse(localStorage.getItem("favorite"))
    const newFavorites = favorite.filter((product) => {
        return id !== product.id
    })

    dispatch(getProducts());

    localStorage.setItem("favorite", JSON.stringify(newFavorites))
    const product = products.find((product) => product.id === id)
            return {
                type: TOGGLE_PRODUCT_TO_FAVORITES,
                payload: product
            }
}
export const removeFromCart = (id) => (dispatch) =>{
    const state = store.getState();
    const products = state.product.products;
    
    const productToDelete = JSON.parse(localStorage.getItem('cart'))
    const newCart = productToDelete.filter((product) => {
        return id !== product.id
    })

    dispatch(getProducts());

    localStorage.setItem('cart', JSON.stringify(newCart))
    const oneProduct = products.find((product) => product.id === id)
        return {
            type: TOGGLE_PRODUCT_TO_CART,
            payload: oneProduct
        }   
    }

export const removeAfterBuy = () => (dispatch) => {
    const productsForBuy =  localStorage.getItem('cart');
    const products = JSON.parse(productsForBuy);
    
        dispatch(getProducts());
        
        if(products){
            localStorage.removeItem('cart')
        }  
}



export const getProducts = () => async (dispatch) => {
    dispatch(getProductsInit())
    if (!localStorage.getItem("cart") || !localStorage.getItem("favorite"))  {
        localStorage.setItem("cart", JSON.stringify([]))
        localStorage.setItem("favorite", JSON.stringify([]))
        }
        try{
            const response = await fetch("./Product.json")
            const data = await response.json()
            const localCart = JSON.parse(localStorage.getItem("cart"))
            const localFavorite = JSON.parse(localStorage.getItem("favorite"))
            const updatedProducts = checkProducts(data.products , localCart , localFavorite)
            updatedStorage(updatedProducts)
            dispatch(getProductsSuccess(updatedProducts))
        } catch(error){
            dispatch(getProductsError(error))
        }
}

