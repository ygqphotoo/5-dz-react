import {GET_PRODUCTS_INIT, 
        GET_PRODUCTS_ERROR, 
        GET_PRODUCTS_SUCCESS, 
        TOGGLE_PRODUCT_TO_CART, 
        TOGGLE_PRODUCT_TO_FAVORITES
        } from "./types"

const initialState = {
    products: [],
    productLoad: false,
    error: null,
}

export const productReducer = (state = initialState, action) => {
    switch (action.type){
        case GET_PRODUCTS_INIT: 
        return {
            ...state,
            productLoad: true,
            error: null
        }
        case GET_PRODUCTS_SUCCESS:
            return{
                ...state,
                productLoad: false,
                products: action.payload
            }
        case  GET_PRODUCTS_ERROR:
            return{
                ...state,
                error: action.error
            }   
        case TOGGLE_PRODUCT_TO_CART:
            return{
                ...state,
                products: state.products.map(product => {
                    if(product.id === action.payload.id){
                        return {
                            ...product,
                            isInCart: !product.isInCart
                        }
                    } else {
                        return product
                    }
                    
                })
            }    
        case TOGGLE_PRODUCT_TO_FAVORITES:
            return{
                ...state,
                products: state.products.map(product => {
                    if (product.id === action.payload.id) {
                        return {
                            ...product,
                            isInFavorite: !product.isInFavorite
                        }
                    } else {
                        return product
                    }
                })
            }  
           default: 
            return state   
        }
}
