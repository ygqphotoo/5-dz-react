import {combineReducers} from "redux";
import {productReducer} from "./products/reducer";
import {modalReducer} from "./modal/reducer";


export const rootReducer = combineReducers ({modal: modalReducer, product: productReducer}) 